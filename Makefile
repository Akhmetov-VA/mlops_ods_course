.PHONY: download-data setup-kaggle build-prod run-prod build-dev run-dev

KAGGLE_USERNAME=$(shell echo $$KAGGLE_USERNAME)
KAGGLE_KEY=$(shell echo $$KAGGLE_KEY)
DATASET=new-york-city/ny-2015-street-tree-census-tree-data

setup-kaggle:
	@echo "Setting up Kaggle API key..."
	mkdir -p $(HOME)/.kaggle
	@echo '{"username":"$(KAGGLE_USERNAME)","key":"$(KAGGLE_KEY)"}' > $(HOME)/.kaggle/kaggle.json
	chmod 600 $(HOME)/.kaggle/kaggle.json

download-data: setup-kaggle
	@echo "Downloading dataset..."
	kaggle datasets download -d $(DATASET) -p data/
	@echo "Unzipping dataset..."
	unzip -o data/$(notdir $(DATASET)).zip -d data
	@echo "Data downloaded and extracted to ./data/"

# Названия образов
PROD_IMAGE_NAME=test:prod
DEV_IMAGE_NAME=test:dev

# Пути
NOTEBOOKS_DIR=$(PWD)/notebooks

# Сборка продакшн образа
build-prod:
	@echo "Building production Docker image..."
	docker build --target production -t $(PROD_IMAGE_NAME) .

# Запуск продакшн образа
run-prod: build-prod
	@echo "Running production Docker image..."
	docker run --rm $(PROD_IMAGE_NAME)

# Сборка разработческого образа
build-dev:
	@echo "Building development Docker image..."
	docker build --target development -t $(DEV_IMAGE_NAME) .

# Запуск разработческого образа
run-dev: build-dev
	@echo "Running development Docker image with Jupyter Notebook..."
	docker run -it -p 8888:8888 -v $(NOTEBOOKS_DIR):/app/notebooks $(DEV_IMAGE_NAME)
