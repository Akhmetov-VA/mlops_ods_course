# MLOps ODS Course

Этот репозиторий служит учебным целям курса по MLOps 3.0. Здесь вы найдете все что я сделал для изучения основ MLOps и практической работы с инструментами автоматизации и управления машинным обучением.

## Начало работы

Чтобы начать работать с проектом, выполните следующие шаги:

1. Клонируйте репозиторий:
    ```bash
    git clone <URL репозитория>
    ```
2. Установите зависимости с помощью [Poetry](https://python-poetry.org/):
    ```bash
    poetry install
    ```
    Это установит все необходимые зависимости, включая `ruff`, `pre-commit` и `mypy`.

3. Активируйте pre-commit хуки для автоматического линтинга и форматирования кода:
    ```bash
    pre-commit install
    ```
4. Cконфигурируйте подключение с помощью make
    ```bash
    export KAGGLE_USERNAME='your_username'
    export KAGGLE_KEY='your_api_key'

    make download-data
    ```
    Это скчает данные о деревьях с кагл и распакует их в директорию .data


## Использование Docker

### Сборка и запуск продового образа
```bash
docker build --target production -t test:prod .
docker run --rm test:prod
```
или использовать make
```bash
make run-prod  # Собирает и запускает продакшн контейнер
```
В итоге запустится скрипт test.py

### Сборка и запуск дев образа
```bash
docker build --target development -t test:dev .
docker run -it -p 8888:8888 -v $(pwd)/notebooks:/app/notebooks test:dev
```
или использовать make
```bash
make run-dev  # Собирает и запускает разработческий контейнер с Jupyter
```
В итоге в контейнере запустится jupyter notebook на порту 8888

## Методология ведения репозитория

В нашем проекте мы применяем методологию Data Science Lifecycle Process (DSLP), которая включает в себя следующие ключевые этапы:

1. **Понимание бизнес-целей и данных**: На этом этапе мы определяем бизнес-задачи, собираем и анализируем необходимые данные.

2. **Предобработка и очистка данных**: Мы проводим очистку и предобработку данных для последующего анализа. Все скрипты и ноутбуки, используемые на этом этапе, организованы в отдельной ветке `data-preprocessing`.

3. **Разведочный анализ данных (EDA)**: На этом этапе мы анализируем данные, используя статистические методы и визуализацию. Результаты EDA публикуются в ветке `exploratory-analysis`.

4. **Моделирование**: Этап, на котором проектируются и тренируются модели машинного обучения. Каждая модель разрабатывается в отдельной ветке, например, `model-xgb` для модели на основе XGBoost.

5. **Оценка и выбор модели**: Модели оцениваются на валидационных данных, лучшая модель выбирается для деплоя. Процесс оценки организован в ветке `model-evaluation`.

6. **Деплой**: Лучшая модель развертывается в продакшен. Код и инструкции для деплоя хранятся в ветке `deployment`.

### Вклад в проект

Мы приветствуем вклад в любом из этапов DSLP. Для начала работы форкните репозиторий, выберите интересующий вас этап и ознакомьтесь с соответствующей документацией в ветках. Перед внесением изменений не забудьте установить зависимости и настроить pre-commit хуки, как описано выше.

Для предложения изменений создайте pull request в соответствующую ветку. Каждый PR будет рассмотрен командой и, при необходимости, обсужден для улучшения качества проекта.

### Стандарты написания кода

Пожалуйста, следуйте стандартам PEP8 и используйте наши инструменты линтинга и форматирования (`ruff`) для обеспечения качества кода. Все инструкции по работе с этими инструментами описаны в [руководстве для контрибьюторов](CONTRIBUTING.md).


## Лицензия

Давно пора сделать лицензию ODS
