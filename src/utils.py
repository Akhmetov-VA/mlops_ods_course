import folium
import pandas as pd
from tqdm import tqdm


def build_map(df: pd.DataFrame, n_values: int = 10_000) -> folium.Map:
    """
    Builds a folium map with circle markers for each row in the dataframe.

    Args:
    df (pd.DataFrame): A dataframe containing the latitude and longitude.
    n_values (int, optional): The number of values to sample from the dataframe. Defaults to 10,000.

    Returns:
    folium.Map: A folium map object with added circle markers.
    """
    # Initialize a map centered around the average location
    map_ = folium.Map(
        location=[df["latitude"].mean(), df["longitude"].mean()], zoom_start=13
    )

    # Add circle markers to the map
    for idx, row in tqdm(df.sample(n=n_values).iterrows(), total=n_values):
        if not pd.isna(row["latitude"]) and not pd.isna(row["longitude"]):
            folium.CircleMarker(
                location=[row["latitude"], row["longitude"]],
                radius=row["tree_dbh"] / 200,
                color="blue",
                fill=True,
                fill_opacity=0.5,
            ).add_to(map_)

    return map_


# Example usage:
if __name__ == "__main__":
    # Assume df is a pandas DataFrame with latitude, longitude, and tree_dbh columns
    df_example = pd.DataFrame(
        {
            "latitude": [40.7128, 40.7062, 40.7152],
            "longitude": [-74.0060, -74.0101, -74.0090],
            "tree_dbh": [10, 20, 15],
        }
    )

    map_created = build_map(df_example)
    map_created.save("map.html")
