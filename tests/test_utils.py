import folium
import pandas as pd

from src.utils import build_map


def test_build_map():
    # Create a sample dataframe
    df_test = pd.DataFrame(
        {
            "latitude": [40.7128, 40.7062],
            "longitude": [-74.0060, -74.0101],
            "tree_dbh": [10, 20],
        }
    )

    # Run the build_map function
    map_test = build_map(df_test, n_values=2)

    # Check if the map has been created and contains the correct number of markers
    assert isinstance(map_test, folium.Map)
    assert len(map_test._children) == 3  # Assuming each CircleMarker is a child
